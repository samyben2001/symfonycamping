<?php

namespace MainBundle\Controller;

use MainBundle\Entity\User;
use MainBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends Controller
{

    public function registerAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $u = new User();

        $form = $this->createForm(UserType::class, $u);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();

            $u->setUserRoles(["ROLE_USER"]);
            $u->setUserSalt(uniqid());
            $encoded = $encoder->encodePassword($u, $u->getUserPassword());
            $u->setUserPassword($encoded);

            $em->persist($u);

            $em->flush();
            $this->addFlash("success", "Utilisateur " . $u->getUserLastName() . " ajouté!");
        }

        return $this->render('MainBundle:User:register.html.twig', array("form" => $form->createView()));
    }

    public function loginAction()
    {
        return $this->render('MainBundle:User:login.html.twig');
    }

}
