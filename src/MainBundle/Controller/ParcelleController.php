<?php

namespace MainBundle\Controller;

use MainBundle\Entity\Parcelle;
use MainBundle\Form\ParcelleFormType;
use MainBundle\Utils\FileService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ParcelleController extends Controller
{
    public function createAction(Request $request, FileService $fileService)
    {
        $p = new Parcelle();

        $form = $this->createForm(ParcelleFormType::class, $p);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($p);

            $picM = $p->getParcelleMainPicture();
            $url = $fileService->moveuploadedFile($picM->getFile());
            $picM->setPictureURL($url);
            $picM->setPictureParcelle($p);
            $em->persist($picM);

            foreach ($p->getParcelleSecondaryPictures() as $pic) {
                $url = $fileService->moveuploadedFile($pic->getFile());
                $pic->setPictureURL($url);
                $pic->setPictureParcelle($p);
                $em->persist($pic);
            }

            $em->flush();
            $this->addFlash("success", "Parcelle ajoutée à l'emplacement " . $p->getParcelleLocation() . "!");
        }
        return $this->render('MainBundle:Parcelle:createParcelle.html.twig', array("form" => $form->createView()));
    }

    public function viewAllAction(){
        $repo = $this->getDoctrine()->getRepository("MainBundle:Parcelle");
        $parcelles = $repo->findAll();

        return $this->render('MainBundle:Parcelle:viewAll.html.twig', array("parcelles" => $parcelles));
    }

    public function updateAction(Request $request, FileService $fileService, Parcelle $p)
    {

        $form = $this->createForm(ParcelleFormType::class, $p);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($p);

            $picM = $p->getParcelleMainPicture();
            $url = $fileService->moveuploadedFile($picM->getFile());
            $picM->setPictureURL($url);
            $picM->setPictureParcelle($p);
            $em->persist($picM);

            foreach ($p->getParcelleSecondaryPictures() as $pic) {
                $url = $fileService->moveuploadedFile($pic->getFile());
                $pic->setPictureURL($url);
                $pic->setPictureParcelle($p);
                $em->persist($pic);
            }

            $em->flush();
            $this->addFlash("success", "Parcelle modifiée à l'emplacement " . $p->getParcelleLocation() . "!");
        }
        return $this->render('MainBundle:Parcelle:updateParcelle.html.twig', array("form" => $form->createView()));
    }

    public function deleteAction(Parcelle $p)
    {
        $em = $this->getDoctrine()->getManager();
        $this->addFlash("error", "Parcelle " . $p->getId() . " supprimée!");
        $em->remove($p);

        $em->flush();
        return $this->redirectToRoute('viewAllParcelle');
    }

    public function detailsAction()
    {
        return $this->render('MainBundle:Parcelle:detailsParcelle.html.twig', array(// ...
        ));
    }

}
