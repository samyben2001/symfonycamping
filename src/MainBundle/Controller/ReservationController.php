<?php

namespace MainBundle\Controller;

use DateTime;
use MainBundle\Entity\Parcelle;
use MainBundle\Entity\Reservation;
use MainBundle\Form\ReservationType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ReservationController extends Controller
{

    public function CalculPrixReservationAction(Request $request, Parcelle $p)
    {
        $repo = $this->getDoctrine()->getRepository("MainBundle:Parcelle");
        $parcelle = $repo->find($p->getId());

        $reservation = new Reservation();

        $form = $this->createForm(ReservationType::class, $reservation);
//        $form->handleRequest($request);
//        dump($request);die;

        if ($request->request->get('mainbundle_reservation') != null) {

            $startDate = DateTime::createFromFormat('d/m/Y', ($request->request->get('mainbundle_reservation')['reservationStartDate']));
            $endDate = DateTime::createFromFormat('d/m/Y', ($request->request->get('mainbundle_reservation')['reservationEndDate']));
            $duree = $startDate->diff($endDate);
            $prixTotal = $duree->format('%a') * $p->getParcellePriceDay();

            return new JsonResponse(array(
                "status" => "ok",
                "duree" => $duree->format('%a'),
                "prixTotal" => $prixTotal
            ));

        }

        return $this->render('MainBundle:Reservation:reservation.html.twig', array('form' => $form->createView(), 'parcelle' => $parcelle));
    }

    public function reserveParcelleAction(Request $request, Parcelle $p)
    {

        $startDate = $request->request->get("startDate");
        $endDate = $request->request->get("endDate");
        $prixTotal = $request->request->get("prixTotal");

        $startDate = DateTime::createFromFormat('d/m/Y', $startDate);
        $endDate = DateTime::createFromFormat('d/m/Y', $endDate);

        $reservation = new Reservation();
        $em = $this->getDoctrine()->getManager();
        $reservation->setReservationUser($this->getUser());
        $reservation->setReservationStartDate($startDate);
        $reservation->setReservationEndDate($endDate);
        $reservation->setReservationPrice($prixTotal);
        $reservation->setReservationParcelle($p);
        $p->setParcelleAvailable(0);
        $em->persist($reservation);
        $em->flush();

        $this->addFlash("success", "Réservation effectuée!");
        return $this->redirectToRoute('viewAllParcelle');
    }

}
