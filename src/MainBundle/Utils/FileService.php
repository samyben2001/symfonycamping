<?php
/**
 * Created by PhpStorm.
 * User: Student
 * Date: 06-09-17
 * Time: 13:52
 */

namespace MainBundle\Utils;


use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileService
{

    /**
     * @param UploadedFile $file
     * @return  string
     */
    public function moveuploadedFile(UploadedFile $file)
    {
        $originalName = $file->getClientOriginalName();
        $newName = 'assets/img/' . $originalName.uniqid();
        $file->move(__DIR__ . "/../../../web/assets/img", $newName);
        return $newName;
    }
}