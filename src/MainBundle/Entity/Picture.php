<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Asserts;

/**
 * Picture
 *
 * @ORM\Table(name="picture")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\PictureRepository")
 */
class Picture
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="PictureURL", type="string", length=255)
     */
    private $pictureURL;


    /**
     * @var Parcelle
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Parcelle", inversedBy="parcelleSecondaryPictures", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="cascade")
     *
     */
    private $pictureParcelle;

    /**
     * @var UploadedFile
     * @Asserts\File(maxSize="10M", maxSizeMessage="La taille du fichier dépasse la limite(10mo)")
     * @Asserts\Image(mimeTypes={"image/jpeg", "image/png"}, mimeTypesMessage="Format non valide")
     *
     */
    private $file;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pictureMainURL
     *
     * @param string $pictureMainURL
     *
     * @return Picture
     */
    public function setPictureMainURL($pictureMainURL)
    {
        $this->pictureURL = $pictureMainURL;

        return $this;
    }

    /**
     * Get pictureMainURL
     *
     * @return string
     */
    public function getPictureMainURL()
    {
        return $this->pictureURL;
    }



    /**
     * Set pictureURL
     *
     * @param string $pictureURL
     *
     * @return Picture
     */
    public function setPictureURL($pictureURL)
    {
        $this->pictureURL = $pictureURL;

        return $this;
    }

    /**
     * Get pictureURL
     *
     * @return string
     */
    public function getPictureURL()
    {
        return $this->pictureURL;
    }

    /**
     * Set pictureParcelle
     *
     * @param \MainBundle\Entity\Parcelle $pictureParcelle
     *
     * @return Picture
     */
    public function setPictureParcelle(\MainBundle\Entity\Parcelle $pictureParcelle = null)
    {
        $this->pictureParcelle = $pictureParcelle;

        return $this;
    }

    /**
     * Get pictureParcelle
     *
     * @return \MainBundle\Entity\Parcelle
     */
    public function getPictureParcelle()
    {
        return $this->pictureParcelle;
    }

    /**
     * @param UploadedFile $file
     * @return Picture
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }
}
