<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Asserts;

/**
 * Parcelle
 *
 * @ORM\Table(name="parcelle")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\ParcelleRepository")
 */
class Parcelle
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="ParcelleAvailable", type="boolean")
     */
    private $parcelleAvailable;

    /**
     * @var int
     *
     * @ORM\Column(name="ParcelleLocation", type="integer", nullable=true, unique=true)
     * @Asserts\LessThanOrEqual(value=300,message="Pas d'emplacement après 300")
     * @Asserts\GreaterThan(value=0, message="Emplacement négatif impossible!")
     */
    private $parcelleLocation;

    /**
     * @var float
     *
     * @ORM\Column(name="ParcellePriceDay", type="float", nullable=true)
     * @Asserts\LessThanOrEqual(value=150,message="Prix trop élevé")
     * @Asserts\GreaterThan(value=0, message="Prix négatif impossible!")
     */
    private $parcellePriceDay;


    /**
     * @var Reservation[]
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\Reservation", mappedBy="reservationParcelle")
     */
    private $parcelleReservations;

    /**
     * @var $parcelleOpt
     * @ORM\OneToOne(targetEntity="MainBundle\Entity\ParcelleOpt", cascade={"persist"})
     */
    private $parcelleOpt;

    /**
     * @var ParcelleType
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\ParcelleType", inversedBy="parcelleTypeParcelles", cascade={"persist"})
     */
    private $parcelleType;

    /**
     * @var Picture
     * @ORM\OneToOne(targetEntity="MainBundle\Entity\Picture", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="cascade")
     * @Asserts\Valid()
     */
    private $parcelleMainPicture;

    /**
     * @var Picture[]
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\Picture", cascade={"persist", "remove"}, mappedBy="pictureParcelle")
     * @ORM\JoinColumn(onDelete="cascade")
     * @Asserts\Valid()
     */
    private $parcelleSecondaryPictures;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->parcelleSecondaryPictures = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parcelleAvailable
     *
     * @param boolean $parcelleAvailable
     *
     * @return Parcelle
     */
    public function setParcelleAvailable($parcelleAvailable)
    {
        $this->parcelleAvailable = $parcelleAvailable;

        return $this;
    }

    /**
     * Get parcelleAvailable
     *
     * @return boolean
     */
    public function getParcelleAvailable()
    {
        return $this->parcelleAvailable;
    }

    /**
     * Set parcelleLocation
     *
     * @param integer $parcelleLocation
     *
     * @return Parcelle
     */
    public function setParcelleLocation($parcelleLocation)
    {
        $this->parcelleLocation = $parcelleLocation;

        return $this;
    }

    /**
     * Get parcelleLocation
     *
     * @return integer
     */
    public function getParcelleLocation()
    {
        return $this->parcelleLocation;
    }

    /**
     * Set parcellePriceDay
     *
     * @param float $parcellePriceDay
     *
     * @return Parcelle
     */
    public function setParcellePriceDay($parcellePriceDay)
    {
        $this->parcellePriceDay = $parcellePriceDay;

        return $this;
    }

    /**
     * Get parcellePriceDay
     *
     * @return float
     */
    public function getParcellePriceDay()
    {
        return $this->parcellePriceDay;
    }


    /**
     * Set parcelleOpt
     *
     * @param \MainBundle\Entity\ParcelleOpt $parcelleOpt
     *
     * @return Parcelle
     */
    public function setParcelleOpt(\MainBundle\Entity\ParcelleOpt $parcelleOpt = null)
    {
        $this->parcelleOpt = $parcelleOpt;

        return $this;
    }

    /**
     * Get parcelleOpt
     *
     * @return \MainBundle\Entity\ParcelleOpt
     */
    public function getParcelleOpt()
    {
        return $this->parcelleOpt;
    }

    /**
     * Set parcelleType
     *
     * @param \MainBundle\Entity\ParcelleType $parcelleType
     *
     * @return Parcelle
     */
    public function setParcelleType(\MainBundle\Entity\ParcelleType $parcelleType = null)
    {
        $this->parcelleType = $parcelleType;

        return $this;
    }

    /**
     * Get parcelleType
     *
     * @return \MainBundle\Entity\ParcelleType
     */
    public function getParcelleType()
    {
        return $this->parcelleType;
    }

    /**
     * Set parcelleMainPicture
     *
     * @param \MainBundle\Entity\Picture $parcelleMainPicture
     *
     * @return Parcelle
     */
    public function setParcelleMainPicture(\MainBundle\Entity\Picture $parcelleMainPicture = null)
    {
        $this->parcelleMainPicture = $parcelleMainPicture;

        return $this;
    }

    /**
     * Get parcelleMainPicture
     *
     * @return \MainBundle\Entity\Picture
     */
    public function getParcelleMainPicture()
    {
        return $this->parcelleMainPicture;
    }

    /**
     * Add parcelleSecondaryPicture
     *
     * @param \MainBundle\Entity\Picture $parcelleSecondaryPicture
     *
     * @return Parcelle
     */
    public function addParcelleSecondaryPicture(\MainBundle\Entity\Picture $parcelleSecondaryPicture)
    {
        $this->parcelleSecondaryPictures[] = $parcelleSecondaryPicture;

        return $this;
    }

    /**
     * Remove parcelleSecondaryPicture
     *
     * @param \MainBundle\Entity\Picture $parcelleSecondaryPicture
     */
    public function removeParcelleSecondaryPicture(\MainBundle\Entity\Picture $parcelleSecondaryPicture)
    {
        $this->parcelleSecondaryPictures->removeElement($parcelleSecondaryPicture);
    }

    /**
     * Get parcelleSecondaryPictures
     *
     * @return Picture[]
     */
    public function getParcelleSecondaryPictures()
    {
        return $this->parcelleSecondaryPictures;
    }


    /**
     * Add parcelleReservation
     *
     * @param \MainBundle\Entity\Reservation $parcelleReservation
     *
     * @return Parcelle
     */
    public function addParcelleReservation(\MainBundle\Entity\Reservation $parcelleReservation)
    {
        $this->parcelleReservations[] = $parcelleReservation;

        return $this;
    }

    /**
     * Remove parcelleReservation
     *
     * @param \MainBundle\Entity\Reservation $parcelleReservation
     */
    public function removeParcelleReservation(\MainBundle\Entity\Reservation $parcelleReservation)
    {
        $this->parcelleReservations->removeElement($parcelleReservation);
    }

    /**
     * Get parcelleReservations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParcelleReservations()
    {
        return $this->parcelleReservations;
    }
}
