<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservation
 *
 * @ORM\Table(name="reservation")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\ReservationRepository")
 */
class Reservation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ReservationStartDate", type="date")
     */
    private $reservationStartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ReservationEndDate", type="date")
     */
    private $reservationEndDate;

    /**
     * @var float
     *
     * @ORM\Column(name="ReservationPrice", type="float")
     *
     */
    private $reservationPrice;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\User", inversedBy="userReservations")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private  $reservationUser;

    /**
     * @var Parcelle
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Parcelle", inversedBy="parcelleReservations")
     */
    private $reservationParcelle;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reservationStartDate
     *
     * @param \DateTime $reservationStartDate
     *
     * @return Reservation
     */
    public function setReservationStartDate($reservationStartDate)
    {
        $this->reservationStartDate = $reservationStartDate;

        return $this;
    }

    /**
     * Get reservationStartDate
     *
     * @return \DateTime
     */
    public function getReservationStartDate()
    {
        return $this->reservationStartDate;
    }

    /**
     * Set reservationEndDate
     *
     * @param \DateTime $reservationEndDate
     *
     * @return Reservation
     */
    public function setReservationEndDate($reservationEndDate)
    {
        $this->reservationEndDate = $reservationEndDate;

        return $this;
    }

    /**
     * Get reservationEndDate
     *
     * @return \DateTime
     */
    public function getReservationEndDate()
    {
        return $this->reservationEndDate;
    }

    /**
     * Set reservationPrice
     *
     * @param float $reservationPrice
     *
     * @return Reservation
     */
    public function setReservationPrice($reservationPrice)
    {
        $this->reservationPrice = $reservationPrice;

        return $this;
    }

    /**
     * Get reservationPrice
     *
     * @return float
     */
    public function getReservationPrice()
    {
        return $this->reservationPrice;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reservationParcelle = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set reservationUser
     *
     * @param \MainBundle\Entity\User $reservationUser
     *
     * @return Reservation
     */
    public function setReservationUser(\MainBundle\Entity\User $reservationUser = null)
    {
        $this->reservationUser = $reservationUser;

        return $this;
    }

    /**
     * Get reservationUser
     *
     * @return \MainBundle\Entity\User
     */
    public function getReservationUser()
    {
        return $this->reservationUser;
    }

    /**
     * Set reservationParcelle
     *
     * @param \MainBundle\Entity\Parcelle $reservationParcelle
     *
     * @return Reservation
     */
    public function setReservationParcelle(\MainBundle\Entity\Parcelle $reservationParcelle = null)
    {
        $this->reservationParcelle = $reservationParcelle;

        return $this;
    }

    /**
     * Get reservationParcelle
     *
     * @return \MainBundle\Entity\Parcelle
     */
    public function getReservationParcelle()
    {
        return $this->reservationParcelle;
    }
}
