<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Asserts;

/**
 * ParcelleFormType
 *
 * @ORM\Table(name="parcelle_type")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\ParcelleTypeRepository")
 */
class ParcelleType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ParcelleTypeName", type="string", length=255, unique=true)
     * @Asserts\Length(min="2", minMessage="Nom trop court",
     *     max="30", maxMessage="Nom trop long")
     */
    private $parcelleTypeName;


    /**
     * @var Parcelle[]
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\Parcelle", mappedBy="parcelleType")
     */
    private $parcelleTypeParcelles;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->parcelleTypeParcelles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parcelleTypeName
     *
     * @param string $parcelleTypeName
     *
     * @return ParcelleType
     */
    public function setParcelleTypeName($parcelleTypeName)
    {
        $this->parcelleTypeName = $parcelleTypeName;

        return $this;
    }

    /**
     * Get parcelleTypeName
     *
     * @return string
     */
    public function getParcelleTypeName()
    {
        return $this->parcelleTypeName;
    }

    /**
     * Add parcelleTypeParcelle
     *
     * @param \MainBundle\Entity\Parcelle $parcelleTypeParcelle
     *
     * @return ParcelleType
     */
    public function addParcelleTypeParcelle(\MainBundle\Entity\Parcelle $parcelleTypeParcelle)
    {
        $this->parcelleTypeParcelles[] = $parcelleTypeParcelle;

        return $this;
    }

    /**
     * Remove parcelleTypeParcelle
     *
     * @param \MainBundle\Entity\Parcelle $parcelleTypeParcelle
     */
    public function removeParcelleTypeParcelle(\MainBundle\Entity\Parcelle $parcelleTypeParcelle)
    {
        $this->parcelleTypeParcelles->removeElement($parcelleTypeParcelle);
    }

    /**
     * Get parcelleTypeParcelles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParcelleTypeParcelles()
    {
        return $this->parcelleTypeParcelles;
    }
}
