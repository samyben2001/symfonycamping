<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParcelleOpt
 *
 * @ORM\Table(name="parcelle_opt")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\ParcelleOptRepository")
 */
class ParcelleOpt
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="ParcelleOptWater", type="boolean")
     */
    private $parcelleOptWater;

    /**
     * @var bool
     *
     * @ORM\Column(name="ParcelleOptElectricity", type="boolean")
     */
    private $parcelleOptElectricity;

    /**
     * @var bool
     *
     * @ORM\Column(name="ParcelleOptTV", type="boolean")
     */
    private $parcelleOptTV;

    /**
     * @var bool
     *
     * @ORM\Column(name="ParcelleOptWC", type="boolean")
     */
    private $parcelleOptWC;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parcelleOptWater
     *
     * @param boolean $parcelleOptWater
     *
     * @return ParcelleOpt
     */
    public function setParcelleOptWater($parcelleOptWater)
    {
        $this->parcelleOptWater = $parcelleOptWater;

        return $this;
    }

    /**
     * Get parcelleOptWater
     *
     * @return boolean
     */
    public function getParcelleOptWater()
    {
        return $this->parcelleOptWater;
    }

    /**
     * Set parcelleOptElectricity
     *
     * @param boolean $parcelleOptElectricity
     *
     * @return ParcelleOpt
     */
    public function setParcelleOptElectricity($parcelleOptElectricity)
    {
        $this->parcelleOptElectricity = $parcelleOptElectricity;

        return $this;
    }

    /**
     * Get parcelleOptElectricity
     *
     * @return boolean
     */
    public function getParcelleOptElectricity()
    {
        return $this->parcelleOptElectricity;
    }

    /**
     * Set parcelleOptTV
     *
     * @param boolean $parcelleOptTV
     *
     * @return ParcelleOpt
     */
    public function setParcelleOptTV($parcelleOptTV)
    {
        $this->parcelleOptTV = $parcelleOptTV;

        return $this;
    }

    /**
     * Get parcelleOptTV
     *
     * @return boolean
     */
    public function getParcelleOptTV()
    {
        return $this->parcelleOptTV;
    }

    /**
     * Set parcelleOptWC
     *
     * @param boolean $parcelleOptWC
     *
     * @return ParcelleOpt
     */
    public function setParcelleOptWC($parcelleOptWC)
    {
        $this->parcelleOptWC = $parcelleOptWC;

        return $this;
    }

    /**
     * Get parcelleOptWC
     *
     * @return boolean
     */
    public function getParcelleOptWC()
    {
        return $this->parcelleOptWC;
    }
}
