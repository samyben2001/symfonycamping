<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReservationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reservationStartDate', DateType::class, array(
                'attr' => array
                    ('class' => 'js-datepicker', "value" => date('d/m/Y')),
                'html5' => false,
                'widget' => 'single_text',
                "label" => "Date de début"))
            ->add('reservationEndDate', DateType::class, array(
                'attr' => array
                ('class' => 'js-datepicker'),
                'html5' => false,
                'widget' => 'single_text',
                "label" => "Date de fin"))
            ->add('submit', SubmitType::class, array("label" => "Réserver"));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\Reservation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mainbundle_reservation';
    }
}
