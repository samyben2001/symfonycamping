<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParcelleOptType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('parcelleOptWater',CheckboxType::class,array("label"=>"Eau", "required" => false))
            ->add('parcelleOptElectricity',CheckboxType::class,array("label"=>"Electricité", "required" => false))
            ->add('parcelleOptTV',CheckboxType::class,array("label"=>"TV", "required" => false))
            ->add('parcelleOptWC',CheckboxType::class,array("label"=>"WC", "required" => false));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\ParcelleOpt'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mainbundle_parcelleopt';
    }


}
