<?php

namespace MainBundle\Form;

use MainBundle\MainBundle;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParcelleFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("parcelleType",EntityType::class, array(
                "label"=>false,
                "class"=> "MainBundle\Entity\ParcelleType",
                "choice_label" => "parcelleTypeName"
            ))
            ->add('parcelleOpt', ParcelleOptType::class, array("label"=>false ))
            ->add('parcelleLocation', IntegerType::class, array("label" => "Emplacement"))
            ->add('parcellePriceDay', MoneyType::class, array("label" => "Prix/jour"))
            ->add('parcelleAvailable', CheckboxType::class, array("label" => "Disponible", "required" => false ))
            ->add('parcelleMainPicture', PictureType::class, array("label" =>"Image principale"))
            ->add('parcelleSecondaryPictures', CollectionType::class, array(
                'allow_add' => true,
                'entry_type' => PictureType::class,
                'label' => false,
                "required" => false ))
            ->add("submit", SubmitType::class,array("label"=>"Enregistrer"));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\Parcelle'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mainbundle_parcelle';
    }


}
