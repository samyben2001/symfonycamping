<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userLastName', TextType::class, array("label" => "Nom"))
            ->add('userFirstName', TextType::class, array("label" => "Prénom"))
            ->add('userBirthDate', BirthdayType::class, array("label" => "Date de naissance"))
            ->add('userPassword', RepeatedType::class, array(
                "type"=>PasswordType::class,
                "invalid_message" => "Les mots de passe ne sont pas identiques",
                "first_options" =>array('label'=> "Mot de passe"),
                "second_options"=>array("label"=> "Confirmer mot de passe")
            ))
            ->add('userUsername', TextType::class, array("label" => "Nom d'utilisateur"))
            ->add('submit', SubmitType::class, array("label" => "Enregistrer"));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mainbundle_user';
    }


}
