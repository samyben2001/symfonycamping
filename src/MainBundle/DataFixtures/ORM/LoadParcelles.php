<?php
/**
 * Created by PhpStorm.
 * User: Student
 * Date: 04-09-17
 * Time: 11:56
 */

namespace MainBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use MainBundle\Entity\Parcelle;
use MainBundle\Entity\ParcelleOpt;
use MainBundle\Entity\ParcelleType;
use MainBundle\Entity\Picture;

class LoadParcelles implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        //création option
        $opt1 = new ParcelleOpt();
        $opt1
            ->setParcelleOptElectricity(true)
            ->setParcelleOptTV(true)
            ->setParcelleOptWater(true)
            ->setParcelleOptWC(true);

        $opt2 = new ParcelleOpt();
        $opt2
            ->setParcelleOptElectricity(false)
            ->setParcelleOptTV(false)
            ->setParcelleOptWater(false)
            ->setParcelleOptWC(false);

        $opt3 = new ParcelleOpt();
        $opt3
            ->setParcelleOptElectricity(true)
            ->setParcelleOptTV(false)
            ->setParcelleOptWater(true)
            ->setParcelleOptWC(false);

        $opt4 = new ParcelleOpt();
        $opt4
            ->setParcelleOptElectricity(true)
            ->setParcelleOptTV(false)
            ->setParcelleOptWater(true)
            ->setParcelleOptWC(true);

        //création type
        $type1 = new ParcelleType();
        $type1 ->setParcelleTypeName("Bungalow");

        $type2 = new ParcelleType();
        $type2 ->setParcelleTypeName("Tente");

        $type3 = new ParcelleType();
        $type3 ->setParcelleTypeName("Caravanne");

        //création image
        $picC = new Picture();
        $picC->setPictureURL("assets/img/defaultCaravanne.jpg");

        $picC2 = new Picture();
        $picC2->setPictureURL("assets/img/defaultCaravanne.jpg");

        $picT = new Picture();
        $picT->setPictureURL("assets/img/defaultTent.jpg");

        $picB = new Picture();
        $picB->setPictureURL("assets/img/defaultBungalow.jpg");


        //création parcelle
        $parcelle1 = new Parcelle();
        $parcelle1
            ->setParcelleAvailable(true)
            ->setParcelleLocation(1)
            ->setParcelleOpt($opt4)
            ->setParcelleType($type3)
            ->setParcellePriceDay(25)
            ->setParcelleMainPicture($picC);

        $parcelle2 = new Parcelle();
        $parcelle2
            ->setParcelleAvailable(true)
            ->setParcelleLocation(2)
            ->setParcelleOpt($opt2)
            ->setParcelleType($type2)
            ->setParcellePriceDay(8)
            ->setParcelleMainPicture($picT);

        $parcelle3 = new Parcelle();
        $parcelle3
            ->setParcelleAvailable(false)
            ->setParcelleLocation(3)
            ->setParcelleOpt($opt1)
            ->setParcelleType($type1)
            ->setParcellePriceDay(35)
            ->setParcelleMainPicture($picB);

        $parcelle4 = new Parcelle();
        $parcelle4
            ->setParcelleAvailable(true)
            ->setParcelleLocation(4)
            ->setParcelleOpt($opt3)
            ->setParcelleType($type3)
            ->setParcellePriceDay(30)
            ->setParcelleMainPicture($picC2);

        $picC->setPictureParcelle($parcelle1);
        $picT->setPictureParcelle($parcelle2);
        $picB->setPictureParcelle($parcelle3);
        $picC2->setPictureParcelle($parcelle4);

        $manager->persist($parcelle1);
        $manager->persist($parcelle2);
        $manager->persist($parcelle3);
        $manager->persist($parcelle4);
        $manager->flush();
    }
}