<?php
/**
 * Created by PhpStorm.
 * User: Student
 * Date: 04-09-17
 * Time: 11:56
 */

namespace MainBundle\DataFixtures\ORM;


use DateTime;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use MainBundle\Entity\User;

class LoadUsers implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $admin = new User();


        $admin
            ->setUserFirstName("Samy")
            ->setUserUsername("admin")
            ->setUserLastName("Ben Beya")
            ->setUserBirthDate(new DateTime())
            ->setUserRoles(["ROLE_ADMIN"])
            ->setUserSalt("8R2rHGYjWRsNIhUSIXkjqbWeQ3Pl+OfXucVEpmqe")
            ->setUserPassword("HEQu1mKrYAl6pAbI4a5hIw+qajmnVb1whHRcoMxZSs+AGMY+ekDiFu3UtJGfO7YroTYTiJFEoxKhV2OesraYNQ==");

        $user = new User();

        $user
            ->setUserFirstName("Khun")
            ->setUserLastName("Ly")
            ->setUserBirthDate(new DateTime())
            ->setUserUsername("user")
            ->setUserRoles(["ROLE_USER"])
            ->setUserSalt("9j23siCblS4m19rwgoOG/P1z4S3u++it1lQ17zBq")
            ->setUserPassword("gMYHZJV9v3AfOCACQrv8M9eDbh6DL5bQmzU3Skpe5m7/xK4AFAg7+0w4UwVHm5mbxpAVm3yiJ+KMGjlg9aQ2Nw==");


        $manager->persist($admin);
        $manager->persist($user);
        $manager->flush();
    }
}