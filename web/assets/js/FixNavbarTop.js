/**
 * Created by Student on 20-09-17.
 */

$(document).ready(function () {
    var offset = $(".navbar").offset().top + $(".navbar").height();
    $(document).scroll(function () {
        var scrollTop = $(document).scrollTop();
        if (scrollTop > offset) {
            $(".navbar").css({"position": "fixed", "width":"100%", "top":"0"});
        }
        else {
            $(".navbar").css("position", "relative");
        }
    });
});