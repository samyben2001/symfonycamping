/**
 * Created by Student on 16-08-17.
 */
var buttonOpen = document.querySelector("#btnModal");
var buttonClose = document.querySelector("#close-modal");
var modalBackground = document.querySelector("#modal-background");
var modalWindow = document.querySelector("#modal-window");

function OpenModal() {
    modalWindow.style.animationName = "OpenModal";
    modalBackground.style.display = "block";
}

function CloseModal() {
    modalWindow.style.animationName = "CloseModal";
    setTimeout(function(){
        modalBackground.style.display = "none";
    }, 550);
}

buttonOpen.addEventListener("click",OpenModal);
buttonClose.addEventListener("click",CloseModal);