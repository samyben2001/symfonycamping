/**
 * Created by Student on 22-09-17.
 */
jQuery(document).ready(function() {
    $('.js-datepicker').datepicker({
        format: "dd/mm/yyyy",
        language: "fr-FR",
        todayHighlight:true,
        startDate: '+0d',
        autoclose:true
    });
});